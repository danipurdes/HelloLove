- Blastium -

How to run:
Locate the file named Blastium and double-click

Summary:
A topdown space shooter inspired by Asteroids

Controls (keyboard):
  Movement:
    W - accelerate
    S - brake/decelerate
    A - turn left/counter-clockwise
    D - turn right/clockwise

  Weapons:
    up   - fire blaster
    left - fire moonshot

  Other:
    P - pause game
    M - mute

Credits:
  Developer - Daniel Purdes
  Technical Consultant - Scott Munro
  Fonts - Chris Early @chriswearly

Engine:
  Love2d - https://love2d.org/

Site:
  https://donnypueblo.itch.io/blastium
